FROM python
COPY serv.py /serv.py
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
CMD python3 /serv.py