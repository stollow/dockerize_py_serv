import time

import redis
from flask import Flask, request

app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

@app.route('/recording', methods=['POST']) 
def save_records():
    request.get_data()
    data = request.data
    ts = time.time()
    cache.set(ts,str(data))
    return data

@app.route('/')
def hello():
    values = []
    keys = cache.keys()
    for key in keys :
        values.append(cache.get(key).decode("utf-8"))
    return str(values)

if __name__ == "__main__":
    app.run(host='0.0.0.0')